package constants;

/**
 *
 * @author Drago - Dragohe4rt
 */
public class CPQTextConstants {
    public static String CPQBlue = "Maple Blue";
    public static String CPQError = "Maple Red";
    public static String CPQEntry = " left the Carnival of Monsters.";
    public static String CPQFindError = "The time has been extended.";
    public static String CPQRed = "Could not find the Leader.";
    public static String CPQPlayerExit = "There was a problem. Please re-create a room.";
    public static String CPQEntryLobby = "Sign up for the Monster Festival!\r\n";
    public static String CPQPickRoom = "The group is currently facing a challenge.";
    public static String CPQExtendTime = "A challenge has been sent to the group in the room. Please wait a while.";
    public static String CPQLeaderNotFound = "We could not find a group in this room.\r\nProbably the group was scrapped inside the room!";
    public static String CPQChallengeRoomAnswer = "You will now receive challenges from other groups. If you do not accept a challenge within 3 minutes, you will be taken out.";
    public static String CPQChallengeRoomSent = "You can select \"Summon Monsters \", \"Ability \", or \"Protector \" as your tactic during the Monster Carnival. Use Tab and F1 ~ F12 for quick access!";
}